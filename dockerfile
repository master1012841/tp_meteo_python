# Utiliser une image Python officielle comme base
FROM python:3.9-slim

# Installer les dépendances du script
RUN pip install requests pymongo

# Copier le script et le fichier JSON dans l'image
COPY scriptPython.py /app/
COPY cities.json /app/

# Définir le répertoire de travail
WORKDIR /app/

# Exécuter le script Python lorsque le conteneur démarre
CMD ["python", "scriptPython.py"]