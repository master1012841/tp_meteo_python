import os
import requests
import json
from pymongo import MongoClient

# Charger le fichier JSON contenant les données des villes
def donnees_villes(chemin_fichier):
    with open(chemin_fichier, 'r', encoding='utf-8') as fichier:
        villes = json.load(fichier)
    return villes

# Trouver les coordonnées d'une ville spécifique
def search_coordonnees(villes, nom_ville, pays):
    for ville in villes:
        if ville['name'].lower() == nom_ville.lower() and ville['country'].lower() == pays.lower():
            return ville['coord']
    return None

# Fonction principale pour obtenir la météo
def obtenir_meteo(lat, lon, api_key):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}"
    response = requests.get(url)
    return response.json()

# Enregistrer les données dans MongoDB
def save_mongodb(data):
    client = MongoClient("mongodb://172.17.0.2:27017/") 
    db = client["TP_Meteo"]
    collection = db["Donnees"]
    collection.insert_one(data)
    print("Données enregistrées dans MongoDB avec succès.")

# Emplacement du fichier JSON des villes
chemin_fichier = 'cities.json'

# Chargement des données des villes
villes = donnees_villes(chemin_fichier)

pays = os.getenv("PAYS")
ville_recherchee = os.getenv("VILLE")

coordonnees = search_coordonnees(villes, ville_recherchee, pays)

if coordonnees:
    API_KEY = os.getenv("API_KEY")
    meteo = obtenir_meteo(coordonnees['lat'], coordonnees['lon'], API_KEY)
    
    # Enregistrer les données dans MongoDB
    save_mongodb(meteo)
else:
    print("Ville non trouvée. Veuillez vérifier l'orthographe ou le code du pays.")
