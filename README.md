## Installation du projet

pip3 install --upgrade pip
pip3 install requests
pip3 install pymongo


## Lancer mongoDB sur docker:
docker run --name mongodb -p 27017:27017 -d mongodb/mongodb-community-server:latest

## Connaitre l'ip du conteneur mongodb (pour mettre dans le script)
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mongodb

## Lancement du dockerfile
docker build -t tp_meteo .
docker run -e PAYS=FR -e VILLE=Paris -e API_KEY=430264947903dc36f471c78593348961 tp_meteo

